import { Hero } from './../hero/hero';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'availableHeroes'
})
export class AvailableHeroesPipe implements PipeTransform {

  transform(heroes: Hero[]): Hero[] {
    if (heroes == null) return null;
    return heroes.filter(h => h.isAvailable);
  }

}
