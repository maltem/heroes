import { Hero } from './../hero/hero';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchHero'
})

export class SearchHeroPipe implements PipeTransform {
    transform(heroes: Hero[], search: string): Hero[] {
    search = search ? search.toLowerCase() : null;
    
    return search ? 
    heroes.filter(h => 
        h.firstname.toLowerCase().indexOf(search) != -1
    || h.lastname.toLowerCase().indexOf(search) != -1
    || h.shortDescription.toLowerCase().indexOf(search) != -1
)
    : heroes;
  }
}