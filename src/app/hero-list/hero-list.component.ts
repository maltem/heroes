import { Hero } from './../hero/hero';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {

  heroes: Hero[];
  message: string= "yo";
  search: string;
  constructor() { }

dislayMsg(msg: string){
  this.message = msg;
}

  ngOnInit() {
    this.heroes = [
    {
        "id": 1,
        "avatar": "",
        "firstname": "Tony",
        "lastname": "Stark",
        "birthdate": "29-08-1981",
        "shortDescription": "Genius-level & Expert hand-to-hand combat",
        "contact": {
            "email": "iron.man@gmail.com",
            "mobile": "+32484888203"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": false,
        "isActive": true,
        "isFavorite": true
    },
    {
        "id": 2,
        "avatar": "ant-man.jpg",
        "firstname": "Scott",
        "lastname": "Lang",
        "birthdate": "01-01-1978",
        "shortDescription": "Electronics expert",
        "contact": {
            "email": "ant.man@gmail.com",
            "mobile": "+32484111111"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": true,
        "isActive": true
    },
    {
        "id": 3,
        "avatar": "black-widow.jpg",
        "firstname": "Natasha",
        "lastname": "Romanova",
        "birthdate": "21-03-1995",
        "shortDescription": "Spy and master of disguise",
        "contact": {
            "email": "black-widow@gmail.com",
            "mobile": "+32484123456"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": true,
        "isActive": true
    },
    {
        "id": 4,
        "avatar": "thor.jpg",
        "firstname": "Thor",
        "lastname": "Odinson",
        "birthdate": "22-03-1975",
        "shortDescription": "Superhuman strength, speed, endurance, and longevity",
        "contact": {
            "email": "thor@gmail.com",
            "mobile": "+32484123456"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": true,
        "isActive": true
    },
    {
        "id": 5,
        "avatar": "hulk.jpg",
        "firstname": "Hulk",
        "lastname": "",
        "birthdate": "12-08-1985",
        "shortDescription": "Green-skinned, hulking and muscular humanoid",
        "contact": {
            "email": "hulk@gmail.com",
            "mobile": "+32484123456"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": true,
        "isActive": true
    },
    {
        "id": 6,
        "avatar": "catwoman.jpg",
        "firstname": "The Cat",
        "lastname": "",
        "birthdate": "10-09-1996",
        "shortDescription": "Highly skilled gymnast",
        "contact": {
            "email": "catwoman@gmail.com",
            "mobile": "+32484123456"
        },
        "socialMedia": [
            {
                "label": "facebook",
                "icon": "fa-facebook",
                "link": "#"
            },
            {
                "label": "linkedin",
                "icon": "fa-linkedin",
                "link": "#"
            }
        ],
        "isAvailable": true,
        "isActive": true
    }




];
  }

}
