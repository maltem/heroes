import { SearchHeroPipe } from './../hero-list/search-hero.pipe';
import { FormsModule } from '@angular/forms';
import { AvailableHeroesPipe } from './../hero-list/available-heroes.pipe';
import { HeroListComponent } from './../hero-list/hero-list.component';
import { HeroComponent } from './../hero/hero.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    HeroComponent,
    HeroListComponent,
    AvailableHeroesPipe,
    SearchHeroPipe
  ],
  exports: [
    HeroListComponent
  ]
})
export class HeroesModule { }
