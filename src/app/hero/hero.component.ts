import { Input, Output, EventEmitter, Component, OnInit } from '@angular/core';
import { Hero } from './hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  @Input() selectedHero: Hero;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    
  }

  sendMessage(){
    this.notify.emit("hello");
  }

}
