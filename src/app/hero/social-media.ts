export class SocialMedia {
    label: string;
    icon: string;
    link: string;
}