import { SocialMedia } from './social-media';
import { Contact } from './contact';

export class Hero {
    id: number;
    avatar: string;
    firstname: string;
    lastname: string;
    birthdate: string;
    shortDescription: string;
    contact: Contact;
    isAvailable: boolean;
    isActive: boolean;
    isFavorite?: boolean;
    socialMedia: SocialMedia[];
}